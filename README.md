# DarkenKP
DarkenKP utilises the [KeeTheme](https://github.com/xatupal/KeeTheme) plugin to give [KeePass](https://keepass.info/) a less intrusive interface.

Style limitations mentioned on the plugin page apply.

## Installation
Download the latest [release](https://gitlab.com/Brady_The/darkenkp/-/archive/master/darkenkp-master.zip) and extract the folder "DarkenKP" and the file "KeeTheme.ini" to \KeePass Password Safe 2\Plugins. Where applicable back up already existing KeeThemes to prevent loss through overwriting.

## Screenshots
Main Interface  
![Screenshot Example Main Interface](https://gitlab.com/Brady_The/darkenkp/raw/master/Screenshots/MainInterface.png)

Options Menu  
![Screenshot Example Main Interface](https://gitlab.com/Brady_The/darkenkp/raw/master/Screenshots/OptionsMenu.png)
